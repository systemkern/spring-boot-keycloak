[![Source Repo](https://img.shields.io/badge/fork%20on-gitlab-important?logo=gitlab)](https://gitlab.com/systemkern/web-service-template)
[![Gitlab Pipelines](https://gitlab.com/systemkern/web-service-template/badges/master/pipeline.svg)](https://gitlab.com/systemkern/web-service-template/-/pipelines)
[![Twitter @systemkern](https://img.shields.io/badge/follow-%40systemkern-blue?logo=twitter)](https://twitter.com/systemkern)
[![LinkedIn @systemkern](https://img.shields.io/badge/contact%20me-%40systemkern-blue?logo=linkedin)](https://linkedin.com/in/systemkern)


Web Service Template
====================
This repository contains a template for a rest microservice based on kotlin, spring boot, spring security

Getting Started
--------------------
The easiest way to run the service locally is by running the following two commands
```bash
# This command will use a dockerized maven to cache all dependencies in a local .m2 folder.
# This makes them available later for the docker build and significantly speeds up the build 
bin/mvn dependency:go-offline

# Build an run our service and the necessary infrastructure
docker-compose up --build`
```

To log into keycloak use `admin` and `password`
There are two users to use the service which are foo and bar both with password: `password`
