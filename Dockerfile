# syntax=docker/dockerfile:1
##################################################
# When upgrading make sure to use the same jdk version for the dockerized mvn command at bin/mvn
FROM maven:3.8.6-openjdk-11 as builder
MAINTAINER Rainer Kern, Systemkern GmbH rainer@systemkern.com
ARG SKIP_TESTS=true
USER root
WORKDIR /build

# If available use the cache built by the bin/mvn command
ADD .m2 /root/.m2
# Separately load the pom (and thus all maven dependencies) to make use of docker's caching
# We assume that the pom is changed less frequently than the rest of the code
ADD pom.xml /build/pom.xml
RUN mvn dependency:go-offline

# Load everything else and run maven to build the service
ADD . /build
RUN mvn clean package -DskipTests=${SKIP_TESTS}

##################################################
# Separate build-time and run-time
# Make sure to use the same java version for the builder and the dockerized maven command bin/mvn
FROM openjdk:11-jre-slim-buster as runner

WORKDIR /app
COPY --from=builder /build/target/lib /app/lib
COPY --from=builder /build/target/web-service-template-1.jar /app/app.jar

EXPOSE 8080
CMD ["java","-Djava.security.egd=file:/dev/./urandom", "-jar", "/app/app.jar"]
